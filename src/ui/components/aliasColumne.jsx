import React, { Component } from 'react'
import {Popconfirm} from 'antd'
import EditableCell from './editableCell'


export const aliasColumns= [{
    title: 'Alias',
    dataIndex: 'name',
    width: '30%',
    render: (text, record) => (
        <EditableCell
            value={text}
            onChange={this.onCellChange(record.key, 'Alias')}
        />
    ),
}, {
    title: 'balance',
    dataIndex: 'age',
}, {
    title: 'address',
    dataIndex: 'address',
    render: (text, record) => (
        <EditableCell
            value={text}
            onChange={this.onCellChange(record.key, 'Alias')}
        />
    ),
}, {
    title: 'operation',
    dataIndex: 'operation',
    render: (text, record) => {
        return (
            this.state.dataSource.length > 1 ?
                (
                    <Popconfirm title="Sure to delete?" onConfirm={() => this.onDelete(record.key)}>
                        <a href="javascript:;">Delete</a>
                    </Popconfirm>
                ) : null
        );
    },
}];
