import { Form, Icon, Input, Button, Card, Tabs } from 'antd'
// import Recaptcha from 'react-recaptcha';
import { connect } from "react-redux";
import ConnectInfo from './connectInfo'
import ValidationComponent from '../common/validation'
import React from 'react'
import logo from '../../source/img/blockeeper_Blue.svg'
import MetamaskLogin from './metamaskLogin'
import '../../App.css'
import { sanitizeKeyString } from '../../util/inputTransformationHelpers'
import settings from '../../source/settings/application.json'
import GoogleLogin from './googleLogin';
import {loginAction} from '../../redux/actions/userAction'
import { loginByMail } from '../../util/fetchJson'
const FormItem = Form.Item

const TabPane = Tabs.TabPane
window.Buffer = window.Buffer || require('buffer').Buffer

const responseGoogleFailure = (response) => {

}
const Signing = require('ethereumjs-util')
class LoginForm extends React.Component {
	componentDidMount = () => {
		// this.onSignIn()
	}

	handleSubmit = e => {
		e.preventDefault()

		this.props.form.validateFields((err, values) => {
			if (!err) {
				this.props.loginAndSetAddress(this.getAddressFromKey(values.password))
			}
		})
	};

	// onloadCallback = (response) => {
	// 	console.log('this is onloadCallback1')
	// 	loginByMail(response.profileObj).then((response)=>{

	// 		console.log('do something==='+JSON.stringify(response))

	// 	})
	// };

	getAddressFromKey = (key) => {
		// Pull the public key from the signed message.
		let messageBuffer = new Buffer(settings.public.login_key, 'hex')
		let keyBuffer = new Buffer(key, 'hex')

		let { v, r, s } = Signing.ecsign(messageBuffer, keyBuffer)

		const signaturePublicKey = Signing.ecrecover(
			new Buffer(settings.public.login_key, 'hex'),
			v,
			new Buffer(r, 'hex'),
			new Buffer(s, 'hex')
		)

		const trimmedAddress = Signing.publicToAddress(signaturePublicKey).toString('hex').toLowerCase()

		return trimmedAddress
	};

	/**
   *
   *  check the private key
   *
   * @memberof LoginForm
   */
	checkPrivateKey = (rule, value, callback) => {
		if (value) {
			if (sanitizeKeyString(value)) {
				callback()
			} else {
				callback('please check your key!')
			}
		} else {
			callback('Please input your Key!')
		}
	};


	render() {
		const { getFieldDecorator } = this.props.form
		const { loginSuccessful } = this.props;
		return (
			<div>
				<Card
					className="login-card"
					bordered="true"
					cover={<img src={logo} alt="logo" style={{ margin: "auto", width: "90%", height: 80 }} />}>

					<Tabs defaultActiveKey="2" size="large">
						<TabPane tab={<span><Icon type="login" /> Api login</span>} key="2">
							{/* <div className="g-signin2" data-width="wide" data-height="60" data-onsuccess="onSignIn" data-theme="dark" data-longtitle="true"></div> */}
							<MetamaskLogin />

							<div>
								<GoogleLogin
									clientId="567650117804-9baln042qsoo1b1occa7msvg8mp9qjbg.apps.googleusercontent.com"
									buttonText="Login"
									className="g-signin2 loginButtonGoole"
									onSuccess={loginSuccessful}
									onFailure={responseGoogleFailure} />

							</div>

						</TabPane>

						<TabPane tab={<span><Icon type="key" />Private key</span>} key="1">
							<Form onSubmit={this.handleSubmit} className="login-form">
								<FormItem>
									{getFieldDecorator('password', {
										rules: [
											{
												validator: this.checkPrivateKey
											}
										]
									})(
										<Input
											prefix={<Icon type="lock" style={{ fontSize: 13 }} />}
											type="password"
											size="large"
											placeholder="Key"
										/>
									)}
								</FormItem>
								<FormItem>
									<Button type="primary" htmlType="submit" className="loginButton">
										<Icon type="login" />
										Log in
  						</Button>
								</FormItem>
								<FormItem className="">
									You can use your private key login
								</FormItem>
								<FormItem className="validation">
									<ValidationComponent />
								</FormItem>
							</Form>
						</TabPane>

					</Tabs>
				</Card>
			</div >
		)
	}
}

const mapStateToProps = state => {

	return { state};
  };

const mapDispatchToProps = (dispatch, state) => {
	return {
		loginSuccessful: response => {
		
			loginByMail(response.profileObj).then((res) => {
				dispatch(loginAction(res.data.user,response.profileObj.imageUrl))

			})

			//dispatch(loginAction(address));
		}
	};
};
const Login = Form.create()(LoginForm)
export default connect(mapStateToProps,mapDispatchToProps)(Login);
//export default Login
