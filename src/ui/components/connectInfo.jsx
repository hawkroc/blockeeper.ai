import { Divider, Alert } from 'antd';

import QueueAnim from 'rc-queue-anim';
import info from '../../source/info.json'

import React from 'react'

class ConnectInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            message: ""
        };
    }

    componentDidMount() {
        let tmp = "Do you like cryptocurrency? \n Do you like to face the challenges?\n Are you a developer?"
        tmp += " mail: " + info.mail
        tmp += " donation: " + info.address
        this.setState({ message: tmp })

    }


    render() {

        const {

        } = this.props;

        return (


            <QueueAnim>
                <div key='demo1'>
                    <Alert
                        className="altertInfo"
                        message="Do you have dreams?"
                        description={this.state.message}
                        closable="true"
                        banner="true"
                        showIcon
                        type="info"
                    />
                </div>
            </QueueAnim>

        )

    }

}


export default ConnectInfo;


