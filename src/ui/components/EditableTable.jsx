import { Table, Input, Icon, Button, Popconfirm } from 'antd';
import React, { Component } from 'react'
//import EditableCell from '../common/editableCell'
//import {aliasColumns} from './aliasColumne'
const EditableCell = ({ editable, value, onChange }) => (
    <div>
      {editable
        ? <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)} />
        : value
      }
    </div>
  );
  
class EditableTable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            dataSource: [],
            count: 0,
            keyColumns: ""
        };
    }

    componentDidMount = () => {

        let data = [{
            key: '0',
            name: 'Edward King',
            age: '32',
            address: 'London, Park Lane no. 0',
        }, {
            key: '1',
            name: 'Edward King 1',
            age: '32',
            address: 'London, Park Lane no. 1',
        }]
        this.setState({ keyColumns: this.props.keyColumns, dataSource: data, count: data.length })

    }




    labelsColumns = [{
        title: 'Name',
        dataIndex: 'name',
        width: '15%',
        render: (text, record) => (
            <EditableCell
                value={text}
                onChange={this.handleChange(record.key, 'Alias')}
            />
        ),
    }, {
        title: 'Ico',
        dataIndex: 'Ico',
        width: '10%',
    }, {
        title: 'parents',
        dataIndex: 'parents',
        width: '20%',
        render: (text, record) => (
            <EditableCell
                value={text}
                onChange={this.handleChange(record.key, 'Alias')}
            />
        ),
    }, {
        title: 'operation',
        dataIndex: 'operation',
        width: '20%',
        render: (text, record) => {
            return (
                this.state.dataSource.length > 1 ?
                    (
                        <Popconfirm title="Sure to delete?" onConfirm={() => this.onDelete(record.key)}>
                            <a href="javascript:;">Delete</a>
                        </Popconfirm>
                    ) : null
            );
        },
    }];


    taxColumns = [{
        title: 'Name',
        dataIndex: 'name',
        width: '15%',
        render: (text, record) => (
            <EditableCell
                value={text}
                onChange={this.onCellChange(record.key, 'Alias')}
            />
        ),
    }, {
        title: 'rate',
        dataIndex: 'rate',
        width: '10%',
    },
    {
        title: 'operation',
        dataIndex: 'operation',
        width: '20%',
        render: (text, record) => {
            return (
                this.state.dataSource.length > 1 ?
                    (
                        <Popconfirm title="Sure to delete?" onConfirm={() => this.onDelete(record.key)}>
                            <a href="javascript:;">Delete</a>
                        </Popconfirm>
                    ) : null
            );
        },
    }];




    aliasColumns = [{
        title: 'Alias',
        dataIndex: 'name',
        width: '15%',
        render: (text, record) => (
            <EditableCell
                value={text}
                onChange={this.onCellChange(record.key, 'Alias')}
            />
        ),
    }, {
        title: 'balance',
        dataIndex: 'age',
        width: '10%',
    }, {
        title: 'address',
        dataIndex: 'address',
        width: '20%',
        render: (text, record) => (
            <EditableCell
                value={text}
                onChange={this.onCellChange(record.key, 'Alias')}
            />
        ),
    }, {
        title: 'operation',
        dataIndex: 'operation',
        width: '20%',
        render: (text, record) => {
            return (
                this.state.dataSource.length > 1 ?
                    (
                        <Popconfirm title="Sure to delete?" onConfirm={() => this.onDelete(record.key)}>
                            <a href="javascript:;">Delete</a>
                        </Popconfirm>
                    ) : null
            );
        },
    }];


    getDiffColumns = (key) => {
        switch (key) {
            case "alias":

                return this.aliasColumns
                break;

            case "labels":
                return this.labelsColumns
                break;


            default:
                return this.taxColumns
                break;

        }
    }



    onCellChange = (key, dataIndex) => {
        return (value) => {
            console.log('value'+JSON.stringify(value));
            const dataSource = [...this.state.dataSource];
            const target = dataSource.find(item => item.key === key);
            if (target) {
                target[dataIndex] = value;
                this.setState({ dataSource });
            }
        };
    }
    onDelete = (key) => {
        const dataSource = [...this.state.dataSource];
        this.setState({ dataSource: dataSource.filter(item => item.key !== key) });
    }
    handleAdd = () => {
        const { count, dataSource } = this.state;
        let newData = {};


        switch (this.state.keyColumns) {
            case "alias":

                newData = {
                    key: count,
                    name: `Edward King ${count}`,
                    age: 32,
                    address: `London, Park Lane no. ${count}`,
                };
                break;

            case "labels":
                newData = {
                    key: count,
                    name: `Edward King ${count}`,
                    ico: 32,
                    parents: 2,
                };
                break;


            default:
                newData = {
                    key: count,
                    name: `name`,
                    rate: 32,
                };
                break;

        }

        this.setState({
            dataSource: [...dataSource, newData],
            count: count + 1,
        });
    }
    render() {
        const columns = this.getDiffColumns(this.state.keyColumns)
        return (
            <div>
                <Button className="editable-add-btn" onClick={this.handleAdd}>Add</Button >
                <Table bordered dataSource={this.state.dataSource} columns={columns} />
            </div>
        );
    }
}
export default EditableTable;