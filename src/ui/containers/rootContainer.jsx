import React from "react";
import { LocaleProvider,Layout } from "antd";
import { connect } from "react-redux";
import Login from "../components/login";
import { loginAction } from "../../redux/actions/userAction";
import HeaderContentLayout from "./layouts/headerContent"
import BodyContentLayout from "./layouts/bodyContent"
import ConnectInfo from './../components/connectInfo'
const { Header, Footer, Content } = Layout;
class RootContainer extends React.Component {
  render() {
    const { isLogin, language,languageConfig, loginSuccessful,user,img } = this.props;

    return (
      <LocaleProvider>
        {
          isLogin ? (
						<div className="list">
							<HeaderContentLayout {...{ user,img}}/>
							<BodyContentLayout {...{  language ,languageConfig}}/>  
             
						</div>    

        ) : (
          <div>
              <ConnectInfo/>
            <Login {...{ loginSuccessful }} />
         
          </div>
        )}
      </LocaleProvider>
    );
  }
}

const mapStateToProps = state => {
  // TODO:
  let address = state.users.address
  let isLogin = state.users.isLogin
  let language =state.users.language
  let languageConfig=state.users.languageConfig
  let user =state.users.user
  let img=state.users.imgUrl


  return { isLogin, language,languageConfig ,user,img};
};

const mapDispatchToProps = (dispatch, state) => {
  return {
    loginSuccessful: user => {
   
      //dispatch(loginAction(address));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RootContainer);
