import React, { Component } from 'react'

import { Row, Col, Button, Tabs, Icon, Card } from 'antd'
import { connect } from "react-redux";
import EditableTable from '../components/editableTable'

const TabPane = Tabs.TabPane;
class SettingsViews extends Component {
  render() {
    return (
      <div>
        <Row>

          <Col span={11}>
            <Card title="Alias Setting" bordered={false} >
              <EditableTable keyColumns="alias" />
            </Card>
          </Col>

          <Col span={12} offset={1}>
            <Card title="Labels&Tax Setting" bordered={false} >
              <Tabs defaultActiveKey="2">
                <TabPane tab={<span><Icon type="apple" />Labels</span>} key="1">
                  <EditableTable keyColumns="labels" />
                </TabPane>
                <TabPane tab={<span><Icon type="android" />Tax</span>} key="2">
                  <EditableTable />
                </TabPane>
              </Tabs>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col span={8} offset={8}>
            <Button type="dashed" size="large">Dashed</Button>
          </Col>
        </Row>
      </div>
    )
  }
}
const mapStateToProps = state => {
  // TODO:
  let address = state.users.address
  let isLogin = state.users.isLogin
  let language = state.users.language
  let languageConfig = state.users.languageConfig
  let user = state.users.user
  let img = state.users.imgUrl
  console.log('user' + JSON.stringify(user))


  return { isLogin, language, languageConfig, user, img };
};

const mapDispatchToProps = (dispatch, state) => {
  return {
    loginSuccessful: user => {

      //dispatch(loginAction(address));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SettingsViews);
