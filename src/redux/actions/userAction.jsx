import * as actionTypes from './actionTypes'

export const loginAction = (user,img) => ({
	type: actionTypes.LOGIN_KEY,
	payload: user,
	imgUrl:img
})


export const setLanguage = (lg) => ({
	type: actionTypes.SETLANGUAGE,
	payload: lg
})


