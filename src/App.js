import React, {
	Component
} from 'react'
import {
	Provider
} from 'react-redux'
import RootContainer from './ui/containers/rootContainer'
import info from './source/info.json'
import store from '../src/redux/store'

class App extends Component {

	render() {
		console.log("Do you have dreams？\nDo you like cryptocurrency?\nDo you like to face the challenges?\nAre you a developer?") 
		console.log('Technology Stack: %c'+info.tech,"color:blue")
		console.log("Any questions please mail me %c "+info.mail,"color:blue")
		console.log("Donation: %c"+info.address,"color:blue")  
		return (

		
			<Provider store={store}>

				<RootContainer/>
			</Provider>

		)
	}






}

export default App