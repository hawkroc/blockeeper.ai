/**
 * TODO: get rid of all of this. All account/transaction related data sould be mined and
 * obtained from OUR serverside collections.
 *
 */

import axios from 'axios'
const key = 'E9MYVKUN5TNUBH6P4E5IWEUHAXGZCXQSNV'
const baseurl = 'https://api.etherscan.io/api?module=account&'
const baseIP = "http://45.32.120.137:8081/renren-api/api/"
var globleKey = ""
var timeout = ""


const getHeaders = () => {
	let headers = {
		'Authorization': globleKey
	}
	return headers
}


export const updateUser=(json)=>{
	return	axios.post(baseIP + "update", json, getHeaders())
		.then(res => {
			if (res.status == 200 && res.data.msg === "success") {
				console.log('res'+JSON.stringify(res))
				return res
			}

		}).catch((error) => {
			console.log(error);
		});
}

export const loginByMail = (json) => {
return	axios.post(baseIP + "login", json, getHeaders())
		.then(res => {
			if (res.status == 200 && res.data.msg === "success") {
				globleKey = res.data.token
				timeout = res.data.expire
				console.log(res.data.expire)
				console.log(res.data.token)
				return res
			}

		}).catch((error) => {
			console.log(error);
		});
}
/**
 *
 * @param {*} address
 * @param {*} url
 */
const PrivateFetch = (address, url) => {
	let final = url

	return axios
		.get(final)
		.then(response => {
			return response
		})
		.catch(error => {
			throw error
		})
}
/**
 * get Exchange from cryptocompare the default type is USD
 */
const url = 'https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms='
export const GetExchange = (type = 'USD') => {
	let final = url + type

	return axios
		.get(final)
		.then(response => {
			return response.data
		})
		.catch(error => {
			throw error
		})
}


const balanceUrl =
	'https://api.etherscan.io/api?module=account&action=balance&tag=latest' +
	`&apikey=${key}`
/**
 *
 * @param {*} address
 */
export const GetBalance = address => {
	let final = balanceUrl + `&address=${address}`
	return PrivateFetch(address, final)
}


/**
 * 
 * @param {*} address 
 */
export const GetTranactions = address => {
	// let final = tranactionsUrl + `&address=${address}`
	let startblock = 1
	let endblock = 99999999999
	let tranactionsUrl = baseurl +
		`action=txlist&address=${address}&startblock=${startblock}` +
		`&endblock=${endblock}&sort=asc&apiKey=${key}`
	console.log('tran' + JSON.stringify(tranactionsUrl))
	return PrivateFetch(address, tranactionsUrl)
}


const QUERY_CURRENT_BLOCK = 'http://api.etherscan.io/api?' +
	'module=proxy&action=eth_blockNumber' +
	`&apikey=${key}`

/**
 * Query API for the current head block.
 *    "base_url": "http://api.etherscan.io/api?",
            "api_key": "E9MYVKUN5TNUBH6P4E5IWEUHAXGZCXQSNV"
 */
const getCurrentBlock = () => {
	return axios
		.get(QUERY_CURRENT_BLOCK)
		.then((response) => {
			response.data.result = parseInt(response.data.result, 16)
			return response
		})
}