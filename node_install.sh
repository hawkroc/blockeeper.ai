brew uninstall --ignore-dependencies --force node
brew uninstall --ignore-dependencies --force icu4c
brew install icu4c
brew unlink icu4c && brew link icu4c --force
brew install node
npm install npm@latest -g